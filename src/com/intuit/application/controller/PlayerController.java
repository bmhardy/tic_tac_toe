package com.intuit.application.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.intuit.application.entity.Player;
import com.intuit.application.service.PlayerService;

@RestController
public class PlayerController {

	@Autowired
	private PlayerService playerService;
	
	@RequestMapping("/players")
	public ControllerResponse getAllPlayers() {
		List<Player> players = playerService.getAllPlayers();
		return new ControllerResponse("players", players, 200, null);
	}
	
	// get a player
	@RequestMapping("/player/{id}")
	public ControllerResponse getPlayer(@PathVariable Long id) {
		Player player = playerService.getPlayer(id);
		return new ControllerResponse("player", player, 200, null);
	}

	// TODO allow create and update of general player
//	// create player
//	@PostMapping("/player")
//	public ControllerResponse addPlayer(@RequestBody Player player) {
//		playerService.addPlayer(player);
//		return new ControllerResponse("player", player, 200, null);
//	}
//		
//	// update player
//	@PutMapping("/player")
//	public ControllerResponse upatePlayer(@RequestBody Player player) {
//		playerService.updatePlayer(player);
//		return new ControllerResponse("player", player, 200, null);
//	}
}
