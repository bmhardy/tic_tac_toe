package com.intuit.application.controller;

public class ControllerResponse {
	private NameValue result;
	private Integer status;
	private String message;
	
	public ControllerResponse(String name, Object result, Integer status, String message) {
		this.result = new NameValue(name, result);
		this.status = status;
		this.message = message;
	}

	public NameValue getResult() {
		return result;
	}

	public Integer getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}
	
	class NameValue {
		private String name;
		private Object value;
		public NameValue(String name, Object value) {
			this.name = name;
			this.value = value;
		}
		public String getName() {
			return name;
		}
		public Object getValue() {
			return value;
		}
	}

}
