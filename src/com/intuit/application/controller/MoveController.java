package com.intuit.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.intuit.application.service.MoveService;

@RestController
public class MoveController {

	@Autowired
	private MoveService moveService;
	
	// get all moves
	@RequestMapping(value = "/moves", method = RequestMethod.GET)
	public ControllerResponse getAllMoves() {
		// TODO make this paged
		return new ControllerResponse("moves", moveService.getAllMoves(), 200, null);
	}
}
