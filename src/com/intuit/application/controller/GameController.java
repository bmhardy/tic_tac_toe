package com.intuit.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.intuit.application.entity.Game;
import com.intuit.application.entity.Move;
import com.intuit.application.entity.Player;
import com.intuit.application.rules.Rules;
import com.intuit.application.rules.TTTRules;
import com.intuit.application.service.GameService;
import com.intuit.application.service.MoveService;
import com.intuit.application.service.PlayerService;

@RestController
public class GameController {

	@Autowired
	private GameService gameService;
	@Autowired
	private PlayerService playerService;
	@Autowired
	private MoveService moveService;
	// TODO
//	@Autowired
//	private Rules rules;
	private Rules rules = new TTTRules();
	
	// create a game
	@PostMapping("/game")
	public ControllerResponse addGame(@RequestBody Game game) {
		rules.setInitialBoardState(game);
		gameService.addGame(game);
		return new ControllerResponse("game", game, 200, null);
	}

	// update a specific game
	@PutMapping("/game")
	public ControllerResponse updateGame(@RequestBody Game game) {
		if (game.getGameStarted())
			return new ControllerResponse("game", null, 400, "Sorry. A started game cannot be modified");

		gameService.updateGame(game);
		return new ControllerResponse("game", game, 200, null);
	}

	// get a specific game
	@RequestMapping(value = "/game/{id}", method = RequestMethod.GET)
	public ControllerResponse getGame(@PathVariable Long id) {
		return new ControllerResponse("game", gameService.getGame(id), 200, null);
	}

	// get all games
	@RequestMapping(value = "/games", method = RequestMethod.GET)
	public ControllerResponse getAllGames() {
		return new ControllerResponse("games", gameService.getAllGames(), 200, null);

	}
	
	// create new player / add player to game
	@PostMapping("/game/{gameId}/player")
	public ControllerResponse createPlayer(@PathVariable Long gameId, @RequestBody Player player) {
		Game game = gameService.getGame(gameId);
		boolean nonExistantGame = game == null;
		if (nonExistantGame)
			return new ControllerResponse("player", null, 400, "Cannot create player for invalid game");

		boolean gameCanTakeNoMorePlayers = game.getGameStarted();
		if (gameCanTakeNoMorePlayers)
			return new ControllerResponse("player", null, 400, "No more players allowed");

		if (playerService.getPlayerByEmail(player.getEmail()) != null)
			return new ControllerResponse("player", null, 400, 
				String.format("Player with email %s already exists", player.getEmail()));

		player.addGame(game);
		playerService.addPlayer(player);
		rules.updateStartState(game);
		gameService.updateGame(game);
		return new ControllerResponse("player", player, 200, null);
	}

	// update game / add player to game
	@PutMapping("/game/{gameId}/player")
	public ControllerResponse addPlayerToGame(@PathVariable Long gameId, @RequestBody Player player) {
		Player existingPlayer = playerService.getPlayerByEmail(player.getEmail());
		if (existingPlayer == null)
			return createPlayer(gameId, player);

		Game game = gameService.getGame(gameId);
		boolean nonExistantGame = game == null;
		if (nonExistantGame)
			return new ControllerResponse("player", null, 400, "Cannot create player for invalid game");

		boolean gameCanTakeNoMorePlayers = game.getGameStarted();
		if (gameCanTakeNoMorePlayers)
			return new ControllerResponse("player", null, 400, "No more players allowed");

		boolean playerAlreadyInGame = existingPlayer.getGameIds().contains(gameId);
		if (playerAlreadyInGame)
			return new ControllerResponse("player", null, 400, 
				String.format("Player already in game %d", gameId));
		
		existingPlayer.addGame(game);
		playerService.updatePlayer(existingPlayer);
		rules.updateStartState(game);
		gameService.updateGame(game);
		return new ControllerResponse("player", existingPlayer, 200, null);
	}
	
	// get all game players
	@RequestMapping(value = "/game/{gameId}/players", method = RequestMethod.GET)
	public ControllerResponse getGamePlayers(@PathVariable Long gameId) {
		Game game = gameService.getGame(gameId);
		return new ControllerResponse("players", game.getPlayers(), 200, null);
	}
	
	// create a move for a game player
	@PostMapping("/game/{gameId}/player/{playerId}/move")
	public ControllerResponse createMove(@PathVariable Long gameId, 
			@PathVariable Long playerId, 
			@RequestBody Move move) {
		Game game = gameService.getGame(gameId);
		boolean nonExistantGame = game == null;
		if (nonExistantGame)
			return new ControllerResponse("move", null, 400, "Invalid game");

		Player player = game.getPlayerById(playerId);
		if (player == null)
			return new ControllerResponse("move", null, 400, String.format("Invalid player %d. Not in game %d", playerId, gameId));
		
		move.setGame(game);
		move.setPlayer(player);
		moveService.addMove(move);
		rules.makeMove(game, move);
		gameService.updateGame(game);
		return new ControllerResponse("move", move, 200, null);
	}
	
	// get all game moves
	@RequestMapping(value = "game/{gameId}/moves", method = RequestMethod.GET)
	public ControllerResponse getAllGameMoves(@PathVariable Long gameId) {
		return new ControllerResponse("moves", moveService.getAllMovesByGame(gameId), 200, null);
	}
}