package com.intuit.application.repository;

import org.springframework.data.repository.CrudRepository;

import com.intuit.application.entity.Game;

public interface GameRepository extends CrudRepository<Game, Long> {

}
