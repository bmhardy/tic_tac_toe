package com.intuit.application.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.intuit.application.entity.Move;

public interface MoveRepository extends CrudRepository<Move, Long> {
//	@Query("SELECT m FROM Move m where gameId=?1")
	public List<Move> findMovesByGameId(Long gameId);
}
