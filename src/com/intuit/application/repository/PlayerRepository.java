package com.intuit.application.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.intuit.application.entity.Player;

public interface PlayerRepository extends CrudRepository<Player, Long> {
	public List<Player> findByGames_Id(Long Id);

	public Player findByEmail(String email);
}
