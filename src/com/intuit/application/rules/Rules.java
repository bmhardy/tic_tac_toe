package com.intuit.application.rules;

import com.intuit.application.entity.Game;
import com.intuit.application.entity.Move;

public interface Rules {
	
	public void updateTurn(Game game);
	public void updateStartState(Game game);
	public void makeMove(Game game, Move move);
	public void setInitialBoardState(Game game);
}

