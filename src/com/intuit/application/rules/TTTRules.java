package com.intuit.application.rules;

import com.intuit.application.entity.Game;
import com.intuit.application.entity.Move;
import com.intuit.application.entity.MoveResult;
import com.intuit.application.entity.MoveStatus;
import com.intuit.application.entity.Player;
import com.intuit.application.util.IRandomWrapper;
import com.intuit.application.util.Random;

public class TTTRules implements Rules {
	
	public static final int numPlayersRequired = 2;
	static final int MAX_MOVES = 9;
	static final char[] markerChoices = {'X', 'O'};
	
	private IRandomWrapper random;
	
	public TTTRules() {
		this.random = new Random();
	}
	
	/**
	 * @param move the move to process
	 * This method processes the move and updates the status of the move as described in processMove
	 */
	@Override
	public void makeMove(Game game, Move move) {
		MoveResult result = processMove(game, move);
		move.setMoveStatus(result.getMoveStatus());
		move.setStatusExplaination(result.getReason());
		if (result.getMoveStatus() != MoveStatus.INVALID_MOVE)
			updateTurn(game);
	}

	@Override
	public void setInitialBoardState(Game game) {
		String[] initialBoardState = {"   ", "   ", "   "};
		game.setBoardState(initialBoardState);
	}
	
	/**
	 * If enough players have joined, determine who's turn it is.
	 */
	@Override
	public void updateTurn(Game game) {
		int numPlayers = game.getPlayers().size();
		boolean notEnoughPlayers = numPlayers != numPlayersRequired;
		if (notEnoughPlayers)
			return;
		
		Long idOfPlayerWithTurn = game.getPlayerIdWithTurn();
		boolean noPlayerHasStarted = idOfPlayerWithTurn == null;
		Player playerWithTurn = null;
		if (noPlayerHasStarted) {
			playerWithTurn = game.getPlayers().get(selectStartingPlayer());
		} else {
			int nextPlayerIndex = game.getPlayers().get(0).getId() == idOfPlayerWithTurn ? 1 : 0;
			playerWithTurn = game.getPlayers().get(nextPlayerIndex);
		}
		game.setPlayerIdWithTurn(playerWithTurn);
	}

	/**
	 * Set the game started if enough players have been added to the game.
	 */
	@Override
	public void updateStartState(Game game) {
		if (game.getGameStarted())
			return;
		int numPlayersNeeded = numPlayersRequired - game.getPlayers().size();
		if (numPlayersNeeded == 0) {
			game.setGameStarted(true);
			game.setGameOver(false);
			updateTurn(game);
		}
	}
	
	public void setRandomWrapper(IRandomWrapper random) {
        this.random = random;
    }
	
	
	/**
	 * @return the index of the starting player
	 */
	int selectStartingPlayer() {	
		int firstPlayerNumber = random.nextInt(numPlayersRequired);
		return firstPlayerNumber;
	}
	
	/**
	 * @param y the Cartesian board position
	 * @return and array index
	 */
	public int convertCartesianY(int y) {
		int updatedY = y;
		switch(y) {
		case 0:
			updatedY = 2;
			break;
		case 1:
			updatedY = 1;
			break;
		case 2:
			updatedY = 0;
			break;
		default:
			break;
		}
		return updatedY;
	}
	
	/**
	 * @param x the x portion of the Cartesian board position
	 * @param y the y portion of the Cartesian board position
	 * @param marker the mark to place on the board
	 * @return non null @MoveResult if board position is taken or if 
	 * requested position is out of board boundary
	 */
	MoveResult updateBoard(Game game, String[] boardState, int x, int y, char marker) {
		int Y = convertCartesianY(y);
		char[][]charBoard = new char[3][3];
		try {
			for (int i = 0; i < 3; i++) {
				char[] row = boardState[i].toCharArray();
				for (int j = 0; j < 3; j++) {
					charBoard[i][j] = row[j];
				}
			}
			
			if (charBoard[Y][x] != ' ')
				return new MoveResult(MoveStatus.INVALID_MOVE, "Spot was taken already");
			
			charBoard[Y][x] = marker;
			for (int i = 0; i < 3; i++) {
				boardState[i] = new String(charBoard[i]);
			}
			game.setBoardState(boardState);
			return null;
		} catch (IndexOutOfBoundsException ex) {
			return new MoveResult(MoveStatus.INVALID_MOVE,
				String.format("Move was out of board boundary"));
		}
	}
	
	/**
	 * @param data three consecutive board positions
	 * @return true if all positions contains the same mark of O or X
	 */
	public boolean isWinningData(String data) {
		if ("XXX".equals(data) || "OOO".equals(data))
			return true;
		return false;
	}
	public boolean isWinningData(char[] data) {
		String stringData = new String(data);
		return isWinningData(stringData);
	}
	
	/**
	 * @return true if the board has a winning move, false otherwise.
	 */
	public boolean gameWasWon(String[] boardState) {
		char[] diagonalL2R = new char[3];
		char[] diagonalR2L = new char[3];
		char[] column = new char[3];
		for (int i = 0, j = 2; i < 3; i++, j--) {
			String row = new String(boardState[i]);
			if (isWinningData(row))
				return true;
			diagonalL2R[i] = boardState[i].charAt(i);
			diagonalR2L[i] = boardState[i].charAt(j);
			for (int k = 0; k < 3; k++)
				column[k] = boardState[k].charAt(i); 
			if (isWinningData(column))
				return true;
		}
		if (isWinningData(diagonalL2R))
			return true;
		if (isWinningData(diagonalR2L))
			return true;
		
		return false;
	}
	
	/**
	 * @param mv the move to process
	 * @return the status of the move
	 * 
	 * This method contains the business logic for processing a game move. The result of processing 
	 * a move is a MoveResult which contains a status and a possible status message.
	 * Moves initially have status NOT_EXECUTED. After processing the move, it receives an INVALID_MOVE
	 * status for various reasons including: 
	 * Invalid game, 
	 * Game not started, 
	 * Game already over, 
	 * Playing out of turn, or 
	 * Move out of board boundary.
	 * 
	 * A move receives WINNING_GAME status if the move caused a win.
	 * A move receives GAME_OVER status if there was no winner and the max moves have been executed.
	 */
	public MoveResult processMove(Game game, Move mv) {
		if (mv.getGame().getId() != game.getId())
			return new MoveResult(MoveStatus.INVALID_MOVE, 
				String.format("Move is for wrong game.", 
					game.getId(), mv.getGame().getId()));

		boolean gameStarted = game.getGameStarted();
		if (!gameStarted)
			return new MoveResult(MoveStatus.INVALID_MOVE, 
				String.format("Game %d not started", game.getId()));
		
		boolean gameOver = game.getGameOver();
		if (gameOver)
			return new MoveResult(MoveStatus.INVALID_MOVE, 
				String.format("Game %d was over", game.getId()));
		
		Long idOfPlayerWithTurn = game.getPlayerIdWithTurn();
		boolean playingOutOfOrder = idOfPlayerWithTurn != mv.getPlayerId();
		if (playingOutOfOrder)
			return new MoveResult(MoveStatus.INVALID_MOVE, 
				String.format("Player %d attempting turn when it is player %d turn", 
					mv.getPlayerId(), idOfPlayerWithTurn));
		
		Object obj = game.getBoardState();
		String[] boardState = new String[] { 
				"   ", 
				"   ",
				"   "
			};
		if (obj instanceof String[] && obj != null)
			boardState = (String[]) obj;
		
		MoveResult result = updateBoard(game, boardState, mv.getX(), mv.getY(), getMarker(game));
		if (result != null)
			return result;
		
		int numMoves = game.getNumMoves();
		numMoves++;
		game.setNumMoves(numMoves);
		
		if (gameWasWon(boardState)) {
			game.setWinningPlayerId(idOfPlayerWithTurn);
			game.setGameOver(true);
			return new MoveResult(MoveStatus.WINNING_MOVE, 
				String.format("player %s won", idOfPlayerWithTurn));
		}
		
		boolean gameEnded = numMoves >= MAX_MOVES;
		if (gameEnded) {
			game.setGameOver(true);
			return new MoveResult(MoveStatus.GAME_OVER, "There was no winner");
		}
			
		return new MoveResult(MoveStatus.VALID_MOVE, null);
	}

	/**
	 * @return the correct marker X or O for the current turn.
	 */
	public char getMarker(Game game) {
		char marker = markerChoices[ game.getNumMoves() % numPlayersRequired ];
		return marker;
	}
}

