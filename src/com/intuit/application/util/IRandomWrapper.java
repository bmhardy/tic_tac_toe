package com.intuit.application.util;

public interface IRandomWrapper {
	 int nextInt(int limit);
}
