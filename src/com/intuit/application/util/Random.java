package com.intuit.application.util;

public class Random implements IRandomWrapper {

	java.util.Random random = null;
	
	public Random() {
		this.random = new java.util.Random();
	}

	@Override
	public int nextInt(int limit) {
		return this.random.nextInt(limit);
	}

}
