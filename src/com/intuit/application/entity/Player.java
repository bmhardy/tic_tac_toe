package com.intuit.application.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Player {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String email;
	private String name;
	
	@ManyToMany(mappedBy = "players")
	List<Game> games = new ArrayList<>();
	
	public Player() {
	}

	public Long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Long> getGameIds() {
		List<Long>ids = new ArrayList<>();
		for (int i = 0; i < games.size(); i++) {
			ids.add(games.get(i).getId());
		}
		return ids;
	}
	
	public void addGame(Game game) {
		this.games.add(game);
		game.getPlayers().add(this);
	}
}
