package com.intuit.application.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Move {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Integer x;
	private Integer y;
	private Date timestamp;
	@Enumerated(EnumType.STRING)
	private MoveStatus moveStatus;
	private String statusExplaination;
	
	@ManyToOne
	private Game game;
	
	@ManyToOne
	private Player player;
	
	public Move() {
		timestamp = new Date();
		moveStatus = MoveStatus.NOT_EXECUTED;
		statusExplaination = null;
	}

	public Long getId() {
		return id;
	}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public Long getPlayerId() {
		return player.getId();
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public MoveStatus getMoveStatus() {
		return moveStatus;
	}

	public void setMoveStatus(MoveStatus moveStatus) {
		this.moveStatus = moveStatus;
	}

	public String getStatusExplaination() {
		return statusExplaination;
	}

	public void setStatusExplaination(String statusExplaination) {
		this.statusExplaination = statusExplaination;
	}
}