package com.intuit.application.entity;

public enum MoveStatus {
	NOT_EXECUTED,
	INVALID_MOVE, 
	VALID_MOVE, 
	WINNING_MOVE,
	GAME_OVER
}