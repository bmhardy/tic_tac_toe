package com.intuit.application.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Game {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Long winningPlayerId;
	private Long playerIdWithTurn;
	private Boolean gameStarted;
	private Boolean gameOver;
	private Integer numMoves;
	// TODO make this more generic to handle various types of boards
	private String[] boardState;
	
	@ManyToMany(fetch = FetchType.LAZY)
	private List<Player> players = new ArrayList<Player>();
	
	public Game() {
		numMoves = 0;
		gameStarted = false;
		gameOver = true;
		winningPlayerId = null;
		playerIdWithTurn = null;
	}
	
	public Long getId() {
		return id;
	}
	
	public List<Player> getPlayers() {
		return this.players;
	}
	
	public Player getPlayerById(Long playerId) {
	    for(Player player : players) {
	        if(player != null && player.getId().equals(playerId)) {
	            return player;
	        }
	    }
	    return null;
	}
	
	public Boolean getGameStarted() {
		return gameStarted;
	}

	public void setGameStarted(Boolean gameStarted) {
		this.gameStarted = gameStarted;
	}

	public Boolean getGameOver() {
		return gameOver;
	}

	public void setGameOver(Boolean gameOver) {
		this.gameOver = gameOver;
	}

	public Integer getNumMoves() {
		return numMoves;
	}
	
	public void setNumMoves(Integer numMoves) {
		this.numMoves = numMoves;
	}

	public Long getWinningPlayerId() {
		return this.winningPlayerId;
	}

	public void setWinningPlayerId(Long playerId) {
		this.winningPlayerId = playerId;
	}
	
	public Long getPlayerIdWithTurn() {
		return this.playerIdWithTurn;
	}

	public void setPlayerIdWithTurn(Player player) {
		if (player == null)
			this.playerIdWithTurn = null;
		else
			this.playerIdWithTurn = player.getId();
	}
	
	public String[] getBoardState() {
		return boardState;
	}

	public void setBoardState(String[] boardState) {
		this.boardState = boardState;
	}

}
