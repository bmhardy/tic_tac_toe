package com.intuit.application.entity;

public class MoveResult {

	private MoveStatus moveStatus;
	private String reason;
	public MoveStatus getMoveStatus() {
		return moveStatus;
	}
	public String getReason() {
		return reason;
	}
	public MoveResult(MoveStatus moveStatus, String reason) {
		this.moveStatus = moveStatus;
		this.reason = reason;
	}
}