package com.intuit.application.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intuit.application.entity.Move;
import com.intuit.application.repository.MoveRepository;

@Service
public class MoveService {

	@Autowired
	private MoveRepository moveRepository;
	
	public List<Move> getAllMoves() {
		List<Move> moves = new ArrayList<Move>();
		Iterator<Move> iterator = moveRepository.findAll().iterator();
		while(iterator.hasNext()) {
			Move move = iterator.next();
			moves.add(move);
		}
		return moves;
	}
	
	public List<Move> getAllMovesByGame(Long gameId) {
		List<Move> moves = new ArrayList<Move>();
		Iterator<Move> iterator = moveRepository.findMovesByGameId(gameId).iterator();
		while(iterator.hasNext()) {
			Move move = iterator.next();
			moves.add(move);
		}
		return moves;
	}
	
	public Move getMove(Long id) {
		return moveRepository.findOne(id);
	}
	
	public void addMove(Move move) {
		moveRepository.save(move);
	}
}
