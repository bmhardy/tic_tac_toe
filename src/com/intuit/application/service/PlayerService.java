package com.intuit.application.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intuit.application.entity.Player;
import com.intuit.application.repository.PlayerRepository;

@Service
public class PlayerService {

	@Autowired
	private PlayerRepository playerRepository;
	
	public List<Player> getAllPlayers() {
		List<Player> players = new ArrayList<Player>();
		Iterator<Player> iterator = playerRepository.findAll().iterator();
		while(iterator.hasNext()) {
			Player player = iterator.next();
			players.add(player);
		}
		return players;
	}
	
	public List<Player> getAllPlayersByGame(Long gameId) {
		List<Player> players = new ArrayList<Player>();
		Iterator<Player> iterator = playerRepository.findByGames_Id(gameId).iterator();
		while(iterator.hasNext()) {
			Player player = iterator.next();
			players.add(player);
		}
		return players;
	}
	
	public Player getPlayer(Long id) {
		return playerRepository.findOne(id);
	}
	
	public void addPlayer(Player player) {
		playerRepository.save(player);
	}
	
	public void updatePlayer(Player player) {
		playerRepository.save(player);
	}

	public Player getPlayerByEmail(String email) {
		return playerRepository.findByEmail(email);
	}
}
