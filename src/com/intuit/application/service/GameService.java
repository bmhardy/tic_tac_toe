package com.intuit.application.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intuit.application.entity.Game;
import com.intuit.application.repository.GameRepository;

@Service
public class GameService {

	@Autowired
	private GameRepository gameRepository;
	
	public List<Game> getAllGames() {
		List<Game> games = new ArrayList<Game>();
		Iterator<Game> iterator = gameRepository.findAll().iterator();
		while(iterator.hasNext()) {
			Game game = iterator.next();
			games.add(game);
		}
		return games;
	}
	
	public Game getGame(long id) {
		return gameRepository.findOne(id);
	}
	
	public void addGame(Game game) {
		gameRepository.save(game);
	}

	public void updateGame(Game game) {
		gameRepository.save(game);
	}
}
