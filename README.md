# Tic Tac Toe #

This is a RESTful service that provides the backend to a highly scalable tic tac toe game.

### How do I get set up? ###

* Ensure you have a recent verison of java (like Java8) installed.
* Ensure you have Maven installed.
* After cloning this repository, cd into tic_tac_toe
* run ```mvn install``` to install all required jars and build the application.
* run ```mvn test``` to run all unit tests
* After you built the application, you can run the server application with either of these two commands: ```mvn spring-boot:run``` or ```java -jar target/TicTacToe-0.0.1-SNAPSHOT.jar```
* In order to access the server and play a tic tac toe game, access these urls as described below:

# Post Actions #

* ```http://localhost:8080/game```                  create a game
* ```http://localhost:8080/game/1/player```         create a player in a game
* ```http://localhost:8080/game/1/player/1/move```  create a game more for a player

# GET Actions #
* ```http://localhost:8080/games```                 get all games
* ```http://localhost:8080/game/1```                get game 1
* ```http://localhost:8080/game/1/players```        get the players in game 1
* ```http://localhost:8080/game/1/moves```          get the moves in game 1

# Sample input and responses #
## Create a game
```http://localhost:8080/game```                  ACTION = POST

input
```
{}
```

response
```
{
    "result": {
        "name": "game",
        "value": {
            "id": 1,
            "players": [],
            "rules": null
        }
    },
    "status": 200,
    "message": null
}
```

## Add a player
```http://localhost:8080/game/1/player```         ACTION = POST

input
```
{ "name": "David", "email": "david@yahoo.com"}
```

response

```
{
    "result": {
        "name": "player",
        "value": {
            "id": 2,
            "email": "david@yahoo.com",
            "name": "David",
            "gameIds": [
                1
            ]
        }
    },
    "status": 200,
    "message": null
}
```

## Display game. Can't play yet. There are not enough players.
```http://localhost:8080/game/1```                ACTION = GET

response
```
{
    "result": {
        "name": "game",
        "value": {
            "id": 1,
            "players": [
                {
                    "id": 1,
                    "email": "david@yahoo.com",
                    "name": "David",
                    "gameIds": [
                        1
                    ]
                }
            ],
            "rules": {
                "id": 1,
                "playerIdWithTurn": null,
                "gameStarted": false,
                "gameOver": true,
                "numMoves": 0,
                "boardState": [
                    "   ",
                    "   ",
                    "   "
                ],
                "winningPlayer": null,
                "numberPlayersRequired": 2
            }
        }
    },
    "status": 200,
    "message": null
}
```

## Add another player
```http://localhost:8080/game/1/player```         ACTION = POST

input
```
{ "name": "Eric", "email": "eric@yahoo.com"}
```

## Display game. We can now play. The game has started. Player one goes first.
```http://localhost:8080/game/1```                ACTION = GET

response
```
{
    "result": {
        "name": "game",
        "value": {
            "id": 1,
            "players": [
                {
                    "id": 1,
                    "email": "david@yahoo.com",
                    "name": "David",
                    "gameIds": [
                        1
                    ]
                },
                {
                    "id": 2,
                    "email": "eric@yahoo.com",
                    "name": "Eric",
                    "gameIds": [
                        1
                    ]
                }
            ],
            "rules": {
                "id": 1,
                "playerIdWithTurn": 1,
                "gameStarted": true,
                "gameOver": false,
                "numMoves": 0,
                "boardState": [
                    "   ",
                    "   ",
                    "   "
                ],
                "winningPlayer": null,
                "numberPlayersRequired": 2
            }
        }
    },
    "status": 200,
    "message": null
}
```

## Player 1 makes a move with X at given coordinates
```http://localhost:8080/game/1/player/1/move```  ACTION = POST

input cartesian coordinates
```
{ "x": 0, "y": 0 } 
```

response: VALID_MOVE

```
  {
    "result": {
        "name": "move",
        "value": {
            "id": 1,
            "x": 0,
            "y": 1,
            "timestamp": 1510547307526,
            "moveStatus": "VALID_MOVE",
            "statusExplaination": null,
            "game": {
                "id": 1,
                "players": [
                    {
                        "id": 1,
                        "email": "bmhardy@yahoo.com",
                        "name": "Brian",
                        "gameIds": [
                            1
                        ]
                    },
                    {
                        "id": 2,
                        "email": "david@yahoo.com",
                        "name": "David",
                        "gameIds": [
                            1
                        ]
                    }
                ],
                "rules": {
                    "id": 1,
                    "playerIdWithTurn": 2,
                    "gameStarted": true,
                    "gameOver": false,
                    "numMoves": 1,
                    "boardState": [
                        "   ",
                        "X  ",
                        "   "
                    ],
                    "winningPlayer": null,
                    "numberPlayersRequired": 2
                }
            },
            "playerId": 1
        }
    },
    "status": 200,
    "message": null
}
```

