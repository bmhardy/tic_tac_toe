package com.intuit.application.rules;

import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.intuit.application.entity.Game;
import com.intuit.application.entity.Move;
import com.intuit.application.entity.MoveResult;
import com.intuit.application.entity.MoveStatus;
import com.intuit.application.entity.Player;
import com.intuit.application.rules.TTTRules;
import com.intuit.application.util.IRandomWrapper;

@RunWith(PowerMockRunner.class)
@PrepareForTest({TTTRules.class}) 
class TTTRulesTest {

	private Move move = null;
	private Game game = null;
	private Player player1 = null;
	private Player player2 = null;
	private IRandomWrapper mockRandom;
	private TTTRules rules;
	
	@BeforeEach
	void setUp() throws Exception {
		game = new Game();
		Whitebox.setInternalState(game, "id", 1L);
		player1 = new Player();
		Whitebox.setInternalState(player1, "id", 1L);
		player1.addGame(game);
		player2 = new Player();
		Whitebox.setInternalState(player2, "id", 2L);
		player2.addGame(game);
		move = new Move();
		Whitebox.setInternalState(move, "id", 1L);
		move.setX(0);
		move.setY(0);
		move.setGame(game);
		move.setPlayer(player1);
		
		rules = new TTTRules();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testUpdateStartState() {
		assertEquals(false, game.getGameStarted());
		assertEquals(true, game.getGameOver());
		mockRandom = new MockedRandomWrapper(1);
		rules.setRandomWrapper(mockRandom);
		rules.updateStartState(game);
		assertEquals(true, game.getGameStarted());
		assertEquals(false, game.getGameOver());
		assertEquals(game.getPlayerIdWithTurn(), player2.getId());
		assertEquals(0, game.getNumMoves().intValue());
	}
	
	@Test
	void testUpdateStartState_noEffectAfterStarted() {
		game.setGameStarted(true);
		game.setGameOver(false);
		game.setPlayerIdWithTurn(player1);
		mockRandom = new MockedRandomWrapper(1);
		rules.setRandomWrapper(mockRandom);
		rules.updateStartState(game);
		assertEquals(true, game.getGameStarted());
		assertEquals(false, game.getGameOver());
		assertEquals(game.getPlayerIdWithTurn(), player1.getId());
		assertEquals(0, game.getNumMoves().intValue());
	}
	
	@Test
	void testUpdateStartState_doesNotStartWithTooFewPlayers() {
		game.getPlayers().remove(1);
		game.setGameStarted(false);
		game.setGameOver(true);
		game.setPlayerIdWithTurn(null);
		mockRandom = new MockedRandomWrapper(1);
		rules.setRandomWrapper(mockRandom);
		rules.updateStartState(game);
		assertEquals(false, game.getGameStarted());
		assertEquals(true, game.getGameOver());
		assertNull(game.getPlayerIdWithTurn());
		assertEquals(0, game.getNumMoves().intValue());
	}
	
	@Test
	void testUpdateTurn_playersAlternateWhenTurnUpdated_Player2First() throws Exception {
		mockRandom = new MockedRandomWrapper(1);
		rules.setRandomWrapper(mockRandom);
		rules.updateTurn(game);
		assertEquals(game.getPlayerIdWithTurn(), player2.getId());
		game.setNumMoves(1);
		rules.updateTurn(game);
		assertEquals(game.getPlayerIdWithTurn(), player1.getId());
	}
	
	@Test
	void testUpdateTurn_playersAlternateWhenTurnUpdated_Player1First() throws Exception {
		mockRandom = new MockedRandomWrapper(0);
		
		rules.setRandomWrapper(mockRandom);
		rules.updateTurn(game);
		assertEquals(game.getPlayerIdWithTurn(), player1.getId());
		game.setNumMoves(1);
		rules.updateTurn(game);
		assertEquals(game.getPlayerIdWithTurn(), player2.getId());
	}
	
	@Test
	void testProcessMove_wrongGame() {
		Game differentGame = new Game();
		Whitebox.setInternalState(game, "id", 3L);
		game.setGameStarted(true);
		game.setGameOver(false);
		game.setPlayerIdWithTurn(player1);
		move.setGame(differentGame);
		MoveResult result = rules.processMove(game, move);
		assertEquals(MoveStatus.INVALID_MOVE, result.getMoveStatus());
		assertEquals("Move is for wrong game.", result.getReason());
	}
	
	@Test
	void testProcessMove_wrongPlayer() {
		game.setGameStarted(true);
		game.setGameOver(false);
		game.setPlayerIdWithTurn(player1);
		Player player = new Player();
		Whitebox.setInternalState(player, "id", 100L);
		move.setPlayer(player);
		MoveResult result = rules.processMove(game, move);
		assertEquals(MoveStatus.INVALID_MOVE, result.getMoveStatus());
		assertEquals("Player 100 attempting turn when it is player 1 turn", result.getReason());
	}
	
	@Test
	void testProcessMove_gameNotStarted() {
		game.setGameStarted(false);
		game.setPlayerIdWithTurn(player1);
		move.setPlayer(player1);
		MoveResult result = rules.processMove(game, move);
		assertEquals(MoveStatus.INVALID_MOVE, result.getMoveStatus());
		assertEquals("Game 1 not started", result.getReason());
	}
	
	@Test
	void testProcessMove_validMove() {
		game.setGameStarted(true);
		game.setGameOver(false);
		game.setPlayerIdWithTurn(player1);
		String[] boardState = new String[]{"   ", "   ", "   "};
		game.setBoardState(boardState);
		move.setPlayer(player1);
		MoveResult result = rules.processMove(game, move);
		assertEquals(MoveStatus.VALID_MOVE, result.getMoveStatus());
		String[] expectedBoard = {"   ", "   ", "X  "};
		assertArrayEquals(expectedBoard, (String[]) game.getBoardState());
	}
	
	@Test
	void testProcessMove_winRow() {
		game.setGameStarted(true);
		game.setGameOver(false);
		game.setPlayerIdWithTurn(player1);
		String[] boardState = {"   ", "   ", " XX"};
		game.setBoardState(boardState);
		move.setPlayer(player1);
		MoveResult result = rules.processMove(game, move);
		assertEquals(MoveStatus.WINNING_MOVE, result.getMoveStatus());
		String[] expectedBoard = {"   ", "   ", "XXX"};
		assertArrayEquals(expectedBoard, (String[]) game.getBoardState());
		assertEquals(player1.getId(), game.getWinningPlayerId());
	}
	
	@Test
	void testProcessMove_x2y1() {
		game.setGameStarted(true);
		game.setGameOver(false);
		game.setNumMoves(0);
		game.setPlayerIdWithTurn(player1);
		String[] boardState = {"X  ", "   ", "   "};
		game.setBoardState(boardState);
		move.setPlayer(player1);
		move.setX(2);
		move.setY(1);
		rules.processMove(game, move);
		String[] expectedBoard = {"X  ", "  X", "   "};
		assertArrayEquals(expectedBoard, (String[]) game.getBoardState());
	}
	
	@Test
	void testProcessMove_x1y1() {
		game.setGameStarted(true);
		game.setGameOver(false);
		game.setNumMoves(0);
		game.setPlayerIdWithTurn(player1);
		String[] boardState = {"   ", "  X", "   "};
		game.setBoardState(boardState);
		move.setPlayer(player1);
		move.setX(1);
		move.setY(1);
		rules.processMove(game, move);
		String[] expectedBoard = {"   ", " XX", "   "};
		assertArrayEquals(expectedBoard, (String[]) game.getBoardState());
	}
	
	@Test
	void testProcessMove_firstPlayerGetsMarkerX() {
		game.setGameStarted(true);
		game.setGameOver(false);
		game.setPlayerIdWithTurn(player1);
		String[] boardState = {"   ", "   ", "   "};
		game.setBoardState(boardState);
		move.setPlayer(player1);
		rules.processMove(game, move);
		String[] expectedBoard = {"   ", "   ", "X  "};
		assertArrayEquals(expectedBoard, (String[]) game.getBoardState());
	}
	
	@Test
	void testProcessMove_secondPlayerGetsMarkerO() {
		game.setGameStarted(true);
		game.setGameOver(false);
		game.setNumMoves(1);
		String[] boardState = {"X  ", "   ", "   "};
		game.setBoardState(boardState);
		game.setPlayerIdWithTurn(player1);
		move.setPlayer(player1);
		rules.processMove(game, move);
		String[] expectedBoard = {"X  ", "   ", "O  "};
		assertArrayEquals(expectedBoard, (String[]) game.getBoardState());
	}
	
	@Test
	void testProcessMove_catsGameEndingMove() {
		game.setGameStarted(true);
		game.setGameOver(false);
		game.setNumMoves(9);
		game.setPlayerIdWithTurn(player1);
		move.setPlayer(player1);
		MoveResult result = rules.processMove(game, move);
		assertEquals(MoveStatus.GAME_OVER, result.getMoveStatus());
		assertEquals("There was no winner", result.getReason());
		assertEquals(true, game.getGameOver());
	}
	
	@Test
	void testProcessMove_whenGameOver() {
		game.setGameStarted(true);
		game.setGameOver(true);
		game.setPlayerIdWithTurn(player1);
		move.setPlayer(player1);
		MoveResult result = rules.processMove(game, move);
		assertEquals(MoveStatus.INVALID_MOVE, result.getMoveStatus());
		assertEquals("Game 1 was over", result.getReason());
		assertEquals(true, game.getGameOver());
	}
	
	@Test
	void testProcessMove_replayUsedSpot() {
		game.setGameStarted(true);
		game.setGameOver(false);
		game.setNumMoves(1);
		game.setPlayerIdWithTurn(player1);
		String[] boardState = { "   ", "   ", "X  " };
		game.setBoardState(boardState);
		move.setPlayer(player1);
		MoveResult result = rules.processMove(game, move);
		assertEquals(MoveStatus.INVALID_MOVE, result.getMoveStatus());
		assertEquals("Spot was taken already", result.getReason());
	}
	
	@Test
	void testProcessMove_outOfBounds() {
		game.setGameStarted(true);
		game.setGameOver(false);
		game.setNumMoves(1);
		game.setPlayerIdWithTurn(player1);
		move.setX(10);
		move.setPlayer(player1);
		MoveResult result = rules.processMove(game, move);
		assertEquals(MoveStatus.INVALID_MOVE, result.getMoveStatus());
		assertEquals("Move was out of board boundary", result.getReason());
	}
	
	@Test
	void testConvertCartesianY() {
		assertEquals(2, rules.convertCartesianY(0));
		assertEquals(1, rules.convertCartesianY(1));
		assertEquals(0, rules.convertCartesianY(2));
		assertEquals(9, rules.convertCartesianY(9));
	}
	
	@Test
	void testUpdateBoard() {
		String[] boardState = {"X  ", " X ", "  X"};
		game.setBoardState(boardState);
		Object[] parameters = new Object[]{game, boardState, 0, 1, 'O'};
		try {
			MoveResult result = Whitebox.invokeMethod(rules, "updateBoard", parameters);
			assertNull(result);
			String[] expectedBoardState = {"X  ", "OX ", "  X"};
			assertArrayEquals(expectedBoardState, (String[]) game.getBoardState());
		} catch (Exception e) {
			e.printStackTrace();
			fail("method call failed");
		}
	}
	
	@Test
	void testUpdateBoard_spotOutOfBounds() {
		String[] boardState = {"X  ", " X ", "  X"};
		game.setBoardState(boardState);
		Object[] parameters = new Object[]{game, boardState, 0, 22, 'O'};
		try {
			MoveResult result = Whitebox.invokeMethod(rules, "updateBoard", parameters);
			assertEquals(MoveStatus.INVALID_MOVE, result.getMoveStatus());
			assertEquals("Move was out of board boundary", result.getReason());
			String[] expectedBoardState = {"X  ", " X ", "  X"};
			assertArrayEquals(expectedBoardState, (String[]) game.getBoardState());
		} catch (Exception e) {
			e.printStackTrace();
			fail("method call failed");
		}
	}
	
	@Test
	void testUpdateBoard_spotTaken() {
		String[] boardState = {"X  ", " X ", "  X"};
		game.setBoardState(boardState);
		Object[] parameters = new Object[]{game, boardState, 0, 2, 'O'};
		try {
			MoveResult result = Whitebox.invokeMethod(rules, "updateBoard", parameters);
			assertEquals(MoveStatus.INVALID_MOVE, result.getMoveStatus());
			assertEquals("Spot was taken already", result.getReason());
			String[] expectedBoardState = {"X  ", " X ", "  X"};
			assertArrayEquals(expectedBoardState, (String[]) game.getBoardState());
			assertArrayEquals(expectedBoardState, (String[]) game.getBoardState());
		} catch (Exception e) {
			e.printStackTrace();
			fail("method call failed");
		}
	}
	
	@Test
	void testGetMarker() {
		game.setNumMoves(0);
		assertEquals('X', rules.getMarker(game));
		game.setNumMoves(1);
		assertEquals('O', rules.getMarker(game));
		game.setNumMoves(2);
		assertEquals('X', rules.getMarker(game));
		game.setNumMoves(3);
		assertEquals('O', rules.getMarker(game));
		game.setNumMoves(4);
		assertEquals('X', rules.getMarker(game));
		game.setNumMoves(5);
		assertEquals('O', rules.getMarker(game));
		game.setNumMoves(9);
		assertEquals('O', rules.getMarker(game));
	}
	
	@Test
	void testGameWasWon_diagonalLeftToRight() {
		String[] boardState = {"X  ", " X ", "  X"};
		assertEquals(true, rules.gameWasWon(boardState));
	}
	
	@Test
	void testGameWasWon_diagonalRighToLeft() {
		String[] boardState = {"  X", " X ", "X  "};
		assertEquals(true, rules.gameWasWon(boardState));
	}
	
	@Test
	void testGameWasWon_column1() {
		String[] boardState = {"X  ", "X  ", "X  "};
		assertEquals(true, rules.gameWasWon(boardState));
	}
	
	@Test
	void testGameWasWon_column2() {
		String[] boardState = {" X ", " X ", " X "};
		assertEquals(true, rules.gameWasWon(boardState));
	}
	
	@Test
	void testGameWasWon_column3() {
		String[] boardState = {"  X", "  X", "  X"};
		assertEquals(true, rules.gameWasWon(boardState));
	}
	
	@Test
	void testGameWasWon_row1() {
		String[] boardState = {"XXX", "   ", "   "};
		assertEquals(true, rules.gameWasWon(boardState));
	}
	
	@Test
	void testGameWasWon_row2() {
		String[] boardState = {"   ", "XXX", "   "};
		assertEquals(true, rules.gameWasWon(boardState));
	}
	
	@Test
	void testGameWasWon_row3() {
		String[] boardState = {"   ", "   ", "XXX"};
		assertEquals(true, rules.gameWasWon(boardState));
	}
	
	@Test
	void testIsWinningData() {
		assertTrue(rules.isWinningData("XXX"));
		assertTrue(rules.isWinningData("OOO"));
		assertFalse(rules.isWinningData("OO"));
		assertFalse(rules.isWinningData("X"));
		assertFalse(rules.isWinningData("XOX"));
	}
	@Test
	void testNumPlayersRequired() {
		assertEquals(2, TTTRules.numPlayersRequired);
	}
}

class MockedRandomWrapper implements IRandomWrapper {

	private int nextInt;

	public MockedRandomWrapper(int theInt) {
		this.nextInt = theInt;
	}

	@Override
	public int nextInt(int limit) {
		return nextInt;
	}

}
