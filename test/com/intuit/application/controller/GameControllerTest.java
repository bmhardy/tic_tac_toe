package com.intuit.application.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.powermock.reflect.Whitebox;
import org.springframework.test.util.ReflectionTestUtils;

import com.intuit.application.entity.Game;
import com.intuit.application.entity.Move;
import com.intuit.application.entity.MoveStatus;
import com.intuit.application.entity.Player;
import com.intuit.application.service.GameService;
import com.intuit.application.service.MoveService;
import com.intuit.application.service.PlayerService;

public class GameControllerTest {
	
	private GameController gameController;
	@BeforeEach
	void setUp() throws Exception {
		gameController = new GameController();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testCreatePlayer_sameEmail() {
		long gameId = 1L;
		GameService mockGameService = Mockito.mock(GameService.class);
		Game game = new Game();
		Player player = new Player();
		player.setEmail("david@yahoo.com");
		Whitebox.setInternalState(player, "id", 1L);
		player.addGame(game);
		Whitebox.setInternalState(game, "id", gameId);
		Mockito.when(mockGameService.getGame(gameId)).thenReturn(game);
		ReflectionTestUtils.setField(gameController, "gameService", mockGameService);
		PlayerService mockPlayerService = Mockito.mock(PlayerService.class);
		ReflectionTestUtils.setField(gameController, "playerService", mockPlayerService);
		Mockito.doNothing().when(mockPlayerService).addPlayer(Mockito.any(Player.class));
		Mockito.when(mockPlayerService.getPlayerByEmail(player.getEmail())).thenReturn(player);
		
		Player anotherPlayer = new Player();
		anotherPlayer.setEmail("david@yahoo.com");
		Whitebox.setInternalState(anotherPlayer, "id", 2L);
		ControllerResponse response = gameController.createPlayer(gameId, anotherPlayer);
		assertEquals(400, response.getStatus().intValue());
		assertEquals("Player with email david@yahoo.com already exists", response.getMessage());
	}
	
	@Test
	void testCreatePlayer_noMorePlayersAfterGameStart() {
		long gameId = 1L;
		GameService mockGameService = Mockito.mock(GameService.class);
		Game game = new Game();
		game.setGameStarted(true);
		Whitebox.setInternalState(game, "id", gameId);
		Mockito.when(mockGameService.getGame(gameId)).thenReturn(game);
		ReflectionTestUtils.setField(gameController, "gameService", mockGameService);
		
		Player player3 = new Player();
		player3.setEmail("jim@yahoo.com");
		Whitebox.setInternalState(player3, "id", 3L);
		ControllerResponse response = gameController.createPlayer(gameId, player3);
		assertEquals(400, response.getStatus().intValue());
		assertEquals("No more players allowed", response.getMessage());
	}
	
	@Test
	void testCreatePlayer_gameDoesNotExist() {
		long gameId = 1L;
		GameService mockGameService = Mockito.mock(GameService.class);
		Mockito.when(mockGameService.getGame(gameId)).thenReturn(null);
		ReflectionTestUtils.setField(gameController, "gameService", mockGameService);
		
		Player player3 = new Player();
		player3.setEmail("jim@yahoo.com");
		Whitebox.setInternalState(player3, "id", 3L);
		ControllerResponse response = gameController.createPlayer(gameId, player3);
		assertEquals(400, response.getStatus().intValue());
		assertEquals("Cannot create player for invalid game", response.getMessage());
	}
	
	@Test
	void testCreatePlayer_success() {
		long gameId = 1L;
		GameService mockGameService = Mockito.mock(GameService.class);
		Game game = new Game();
		
		Player player = new Player();
		player.setEmail("david@yahoo.com");
		Whitebox.setInternalState(player, "id", 1L);
		player.addGame(game);
		Whitebox.setInternalState(game, "id", gameId);
		Mockito.when(mockGameService.getGame(gameId)).thenReturn(game);
		ReflectionTestUtils.setField(gameController, "gameService", mockGameService);
		PlayerService mockPlayerService = Mockito.mock(PlayerService.class);
		ReflectionTestUtils.setField(gameController, "playerService", mockPlayerService);
		Mockito.doNothing().when(mockPlayerService).addPlayer(Mockito.any(Player.class));
		Mockito.when(mockPlayerService.getPlayerByEmail(player.getEmail())).thenReturn(player);
		
		Player anotherPlayer = new Player();
		anotherPlayer.setEmail("eric@yahoo.com");
		Whitebox.setInternalState(anotherPlayer, "id", 2L);
		ControllerResponse response = gameController.createPlayer(gameId, anotherPlayer);
		assertEquals(200, response.getStatus().intValue());
		assertEquals(response.getResult().getName(), "player");
		assertEquals(response.getResult().getValue(), anotherPlayer);
	}
	
	@Test
	void testAddPlayerToGame_success() {
		long gameId = 1L;
		GameService mockGameService = Mockito.mock(GameService.class);
		Game game = new Game();
		Whitebox.setInternalState(game, "id", gameId);
		
		Player player = new Player();
		player.setEmail("david@yahoo.com");
		Whitebox.setInternalState(player, "id", 1L);
		
		Mockito.when(mockGameService.getGame(gameId)).thenReturn(game);
		ReflectionTestUtils.setField(gameController, "gameService", mockGameService);
		
		PlayerService mockPlayerService = Mockito.mock(PlayerService.class);
		ReflectionTestUtils.setField(gameController, "playerService", mockPlayerService);
		Mockito.doNothing().when(mockPlayerService).addPlayer(Mockito.any(Player.class));
		Mockito.when(mockPlayerService.getPlayerByEmail(player.getEmail())).thenReturn(player);
		
		Player anotherPlayer = new Player();
		anotherPlayer.setEmail("david@yahoo.com");
		
		ControllerResponse response = gameController.addPlayerToGame(gameId, anotherPlayer);
		assertEquals(200, response.getStatus().intValue());
		assertEquals(response.getResult().getName(), "player");
		Player result = (Player)response.getResult().getValue();
		assertTrue(result.getGameIds().contains(gameId));
	}
	
	@Test
	void testAddPlayerToGame_playerAlreadyInGame() {
		long gameId = 1L;
		GameService mockGameService = Mockito.mock(GameService.class);
		Game game = new Game();
		Whitebox.setInternalState(game, "id", gameId);
		
		Player player = new Player();
		Whitebox.setInternalState(player, "id", 1L);
		player.setEmail("david@yahoo.com");
		player.addGame(game);
		List<Player>players = new ArrayList<Player>();
		players.add(player);
		
		Mockito.when(mockGameService.getGame(gameId)).thenReturn(game);
		ReflectionTestUtils.setField(gameController, "gameService", mockGameService);
		
		PlayerService mockPlayerService = Mockito.mock(PlayerService.class);
		ReflectionTestUtils.setField(gameController, "playerService", mockPlayerService);
		Mockito.doNothing().when(mockPlayerService).addPlayer(Mockito.any(Player.class));
		Mockito.when(mockPlayerService.getPlayerByEmail(player.getEmail())).thenReturn(player);
		
		Player anotherPlayer = new Player();
		anotherPlayer.setEmail("david@yahoo.com");
		Whitebox.setInternalState(anotherPlayer, "id", 2L);
		
		ControllerResponse response = gameController.addPlayerToGame(gameId, anotherPlayer);
		assertEquals(400, response.getStatus().intValue());
		assertEquals("Player already in game 1", response.getMessage());
	}
	
	@Test
	void testCreateMove_gameDoesNotExist() {
		long gameId = 1L;
		long playerId = 1L;
		GameService mockGameService = Mockito.mock(GameService.class);
		Mockito.when(mockGameService.getGame(gameId)).thenReturn(null);
		ReflectionTestUtils.setField(gameController, "gameService", mockGameService);
		
		Move move = new Move();
		move.setX(1);
		move.setY(1);
		Whitebox.setInternalState(move, "id", 3L);
		ControllerResponse response = gameController.createMove(gameId, playerId, move);
		assertEquals(400, response.getStatus().intValue());
		assertEquals("Invalid game", response.getMessage());
	}
	
	@Test
	void testCreateMove_playerNotInGame() {
		long gameId = 1L;
		long playerId = 1L;
		GameService mockGameService = Mockito.mock(GameService.class);
		Game game = new Game();
		Whitebox.setInternalState(game, "id", gameId);
		Mockito.when(mockGameService.getGame(gameId)).thenReturn(game);
		ReflectionTestUtils.setField(gameController, "gameService", mockGameService);
		
		Move move = new Move();
		move.setX(1);
		move.setY(1);
		Whitebox.setInternalState(move, "id", 3L);
		ControllerResponse response = gameController.createMove(gameId, playerId, move);
		assertEquals(400, response.getStatus().intValue());
		assertEquals("Invalid player 1. Not in game 1", response.getMessage());
	}
	
	@Test
	void testCreateMove_successAndProcessMove() {
		long gameId = 1L;
		long playerId = 1L;
		GameService mockGameService = Mockito.mock(GameService.class);
		Game game = new Game();
		
		Whitebox.setInternalState(game, "id", gameId);
		Mockito.when(mockGameService.getGame(gameId)).thenReturn(game);
		ReflectionTestUtils.setField(gameController, "gameService", mockGameService);
		
		Player player = new Player();
		Whitebox.setInternalState(player, "id", playerId);
		player.addGame(game);
		Move move = new Move();
		move.setX(1);
		move.setY(1);
		Whitebox.setInternalState(move, "id", 3L);
		
		MoveService mockMoveService = Mockito.mock(MoveService.class);
		ReflectionTestUtils.setField(gameController, "moveService", mockMoveService);
		Mockito.doNothing().when(mockMoveService).addMove(Mockito.any(Move.class));
		ControllerResponse response = gameController.createMove(gameId, playerId, move);
		assertEquals(200, response.getStatus().intValue());
		assertEquals(response.getResult().getName(), "move");
		Move result = (Move)response.getResult().getValue();
		assertEquals(result.getId(), move.getId());
		assertEquals(MoveStatus.INVALID_MOVE, move.getMoveStatus());
		assertEquals("Game 1 not started", move.getStatusExplaination());
	}
}
